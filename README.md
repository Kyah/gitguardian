# gitguardian

## Questions

1. What would you do differently on a managed cluster (EKS, GKE...) regarding step 4?

Instead of using `kubectl port-forward` to map the service port to a local port of my computer, I would setup a Ingress Controller which can automatically configure our cloud provider's a managed service (like an AWS ALB or an Azure Application Gateway) and use an Ingress object to link a domain (and/or specific URIs) to the application. 

2. How would you monitor the application?

A correct monitoring of this application should consist of multiple thing :
* Metrics : Getting applications specific metrics can help dectect or diagnose problems. Some basic metrics for an application like this one could be the request rate per second, the request duration and the rate of error log messages for each criticity (critical, error, warning, exception)
* Tracing : Having a way to follow each transactions is a must have to pinpoint exactly where a problem occurred. 
* Logging : Good logs are the best way to track error reporting and related data. Coupled with tracing informations, it helps to find what happens with the application at any given time.

All of those can be implemented with many solutions like the LGTM Stack (Loki/Graphana/Tempo/Mimir) or Datadog and needs to be complemented with a good underlying infrastructure monitoring. We could also use Kubernetes's Probes to improve the application lifecycle and limit the needs of a manual intervention in case of failure.

3. What would you change to add a PostgreSQL container?

The biggest challenge with this kind of workload in kubernetes is the persistent storage. If possible, I would first check if we could use a managed service instead (Technically but also cost-wise).
If the best solution is still Kubernetes, I'll add it as Stateful set resource so that we could use the PersistentVolumes resources to ensure data persistence (assuming we have a good replicated storage solution, hopefully a managed one like AWS EBS)

4. Provide us some feedback on this test:
    a. Duration: is it too long, too short or adequate?

    The duration was adequate with the time I could find to answer. 

    b. Difficulty: on a scale from 0 (easy) to 10 (hard), how would you rate the difficulty of this test? 
    
    I found this test rather easy, I'll go with a 1.

    c. Did you find the test interesting? If not, please tell us why.

    This is a good technical test to get an overview of the candidate.

