import bottle
import os
from datetime import datetime

app = bottle.default_app()
startup_time = datetime.now()


@bottle.route("/")
def hello_world():
    return "Hello GitGuardian!"


@bottle.route("/pod-id")
def pod_id():
    return os.getenv("HOSTNAME")


@bottle.route("/startup-time")
def startup():
    return startup_time.astimezone().strftime("%Y-%m-%dT%H:%M:%S %Z")


if __name__ == "__main__":
    bottle.run(host="0.0.0.0", port=8080)
